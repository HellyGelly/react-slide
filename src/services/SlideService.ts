import axios from "axios";
import { SlideData } from "../components/Slides";

const API_URL = 'http://162.19.33.222:10000/api/'

class SlideService {
	get(idDiapo: string) {
		return axios
			.get(API_URL + `diapo/${idDiapo}/slide`)
			.then((response) => {
				return response.data;
			});
	}

	store(idDiapo: string) {
		return axios
			.post(API_URL + `diapo/${idDiapo}/slide/store`)
			.then((response) => {
				return response.data;
			});
	}

	delete(idSlide:number) {
		return axios
			.delete(API_URL + `slide/${idSlide}/delete`)
			.then((response) => {
				return response.data;
			});
	}

	saveAll(slides:SlideData[]) {
		return axios
			.put(API_URL + `slide/save`, {
				slides: slides
			})
			.then((response) => {
				return response.data;
			});
	}

	// update(nickname: string, mail: string, password: string) {
	// 	return axios.post(API_URL + "registration", {
	// 		nickname,
	// 		mail,
	// 		password,
	// 	});
	// }
}

export default new SlideService();

import axios from "axios";

const API_URL = 'http://162.19.33.222:10000/api/'

class SlideService {
	getOneDiapo(idDiapo: string) {
		return axios
			.get(API_URL + `diapo/${idDiapo}`)
			.then((response) => {
				return response.data;
			});
	}
	getDiapoByUser(idUser: number, orderBy: string, search: string) {
		return axios
			.get(API_URL + `user/${idUser}/diapo`, {
				params: {
					orderBy: orderBy,
					search: search? search : null
				}
			})
			.then((response) => {
				return response.data;
			});
	}

	store(idUser: number, titre:string) {
		return axios
			.post(API_URL + `user/${idUser}/diapo/store`, {
				titre: titre
			})
			.then((response) => {
				return response.data;
			});
	}

	// delete(idSlide:number) {
	// 	return axios
	// 		.delete(API_URL + `slide/${idSlide}/delete`)
	// 		.then((response) => {
	// 			return response.data;
	// 		});
	// }

	// update(nickname: string, mail: string, password: string) {
	// 	return axios.post(API_URL + "registration", {
	// 		nickname,
	// 		mail,
	// 		password,
	// 	});
	// }
}

export default new SlideService();

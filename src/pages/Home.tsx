import { useLayoutEffect } from 'react';
import { gsap } from 'gsap';
import circleLarge from '../images/circle-large.png'

const Home = (): JSX.Element => {

  useLayoutEffect(() => {
    gsap.fromTo('#text',
      { x: '30rem', opacity: 0 },
      { x: 0, opacity: 1, ease: 'back.out(1.7)', delay: 1.9 });

    gsap.fromTo('#watch-wrapper',
      { x: '30rem', opacity: 0 },
      { x: 0, opacity: 1, ease: 'back.out(1.7)', delay: 2.3 });

    gsap.fromTo('#circle-small',
      { x: '-30rem', opacity: 0 },
      { x: 0, opacity: 1, ease: 'back.out(1.7)', delay: 1 });

    gsap.fromTo('#circle-large',
      { x: '30rem', opacity: 0 },
      { x: 0, opacity: 1, ease: 'back.out(1.7)', delay: 1.5 });
  }, [])
  return (
    <div className='relative bg-[#000000] overflow-x-hidden'>
      <nav className='absolute top-0 left-0 w-full h-32 flex justify-between items-center px-44 z-[99]'>
        <div className='text-white text-4xl'>REACT SLIDE</div>
        <div className='flex flex-row gap-3'>
          <a href='/app'>
            <button className='text-white hover:bg-[#7053ff] py-2 px-4 border border-[#7053ff] rounded-xl text-sm duration-300'>Accéder à l'application</button>
          </a>
        </div>
      </nav>
      <div className='relative w-full h-screen flex justify-center items-center'>
        <div className='absolute -top-[6rem] -left-[10rem]'>
          <img className='w-96 h-96' src={circleLarge} id="circle-small" />
        </div>

        <div className='absolute -bottom-[60rem] -right-[40rem]'>
          <img src={circleLarge} id="circle-large" />
        </div>

        <div className='relative w-full h-full flex flex-row px-40 min-[1800px]:px-56'>
          <div className='w-2/5 h-full flex flex-col justify-center items-start text-white' id="text">
            <div className='text-6xl py-4 uppercase font-extrabold'>Concevez des présentations avec <span className='text-[#7053ff]'>React Slide</span></div>
            <div className='text-xl py-4'>Créez des présentations en ligne, présentez-les et collaborez dessus en temps réel, depuis n'importe quel appareil.</div>
            <div className='w-full px-16 py-4'>
              <button className='w-full bg-[#7053ff] py-3 hover:bg-[#7053ff]/75 rounded-xl'>Se connecter</button>
            </div>
            <div className='text-sm py-4 w-full text-center'>Vous n'avez pas de compte ? <a href="#" className='text-[#7053ff] hover:text-[#7053ff]/75'>S'inscrire</a></div>
          </div>
          <div className='w-3/5 h-full flex justify-center items-center' id="watch-wrapper">
            <img src='https://lh3.googleusercontent.com/W3HBkgBOukkuEyRDn0nleMCyB_si5PQqmc51J-isU6gnKyj1m0zlzOAhvxbtfZHgALetXEuqJMi7dnDlQIZgJlTwe6Td-qyEYKBnJ3xiTPHTUodW5Oo=s0' />
          </div>
        </div>
      </div>
      <div className='relative w-full flex flex-col justify-center items-center pt-20'>
        <div className='w-4/5 min-[1800px]:w-3/5 h-full flex flex-wrap gap-6 justify-center items-center pb-32'>
          <div className='group relative bg-[#101010]/80 h-[30rem] rounded-3xl flex flex-row w-full border border-neutral-800 shadow-lg overflow-hidden'>
            <div className='w-2/4 h-full flex flex-col justify-center items-center text-white px-16 py-20'>
              <div className='text-4xl py-4 uppercase font-extrabold text-center'>Créez des <span className='text-violet-600'>présentations</span> efficaces et rapide</div>
              <div className='py-4 text-center' >Synchronisez les données de vos présentations grâce aux modifications en temps réel. Créer votre présentation facilement grâce aux outils mis à disposition.</div>
            </div>
            <div className='relative w-2/4 h-full flex justify-center items-center -skew-y-2 duration-500 pr-6'>
              <img className='rounded-lg' src='https://s.tmimgcdn.com/scr/800x500/101200/modele-powerpoint-propre-a-la-technologie_101225-original.jpg' />
            </div>
          </div>
          <div className='w-full flex flex-row gap-6'>
            <div className='group relative bg-[#101010]/80 h-[30rem] rounded-3xl flex flex-row w-1/2 border border-neutral-800 shadow-lg overflow-hidden'>
              <div className='w-full h-full flex flex-col justify-center items-center text-white px-16 py-20'>
                <div className='text-4xl py-4 uppercase font-extrabold text-center'>Présentez des diaporamas en toute confiance</div>
                <div className='py-4 text-center'>Grâce au mode Présentateur convivial, aux commentaires du présentateur et aux sous-titres instantanés, vous pouvez très facilement exposer vos idées. Vous pouvez même le faire dans des appels vidéo Google Meet directement depuis Slides.</div>
              </div>
            </div>
            <div className='group relative bg-[#101010]/80 h-[30rem] rounded-3xl flex flex-row w-1/2 border border-neutral-800 shadow-lg overflow-hidden'>
              <div className='w-full h-full flex flex-col justify-center items-center text-white px-16 py-20'>
                <div className='text-4xl py-4 uppercase font-extrabold text-center'>Exportez votre présentation</div>
                <div className='py-4 text-center'>Exporter facilement votre présentation en PDF.</div>
              </div>
            </div>
          </div>
        </div>
        <div className='w-4/5 h-full flex flex-row gap-6 justify-center items-center pb-32'>
          <div className='group relative rounded-3xl flex flex-row w-full overflow-hidden'>
            <div className='w-full flex flex-col justify-start h-56 items-center text-white px-4 py-1'>
              <div className='py-4 uppercase font-extrabold text-center'>Travaillez sur du contenu actualisé</div>
              <div className='py-4 text-center text-neutral-400'>Avec Slides, tout le monde travaille sur la dernière version de la présentation. Les modifications étant enregistrées automatiquement dans l'historique des versions, vous pouvez les suivre ou les annuler facilement.</div>
            </div>
          </div>
          <div className='group relative rounded-3xl flex flex-row w-full overflow-hidden'>
            <div className='w-full h-56 flex flex-col justify-start items-center text-white px-4 py-1'>
              <div className='py-4 uppercase font-extrabold text-center'>Créez des présentations plus rapidement grâce aux fonctionnalités intelligentes</div>
              <div className='py-4 text-center text-neutral-400'>Les fonctionnalités d'assistance comme la rédaction intelligente et la correction automatique vous aident à créer des présentations plus rapidement avec moins d'erreurs.</div>
            </div>
          </div>
          <div className='group relative rounded-3xl flex flex-row w-full overflow-hidden'>
            <div className='w-full h-56 flex flex-col justify-start items-center text-white px-4 py-1'>
              <div className='py-4 uppercase font-extrabold text-center'>Restez productif, même hors connexion</div>
              <div className='py-4 text-center text-neutral-400'>Vous pouvez consulter, créer et modifier des présentations Slides même sans connexion Internet, pour rester productif où que vous soyez.</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;

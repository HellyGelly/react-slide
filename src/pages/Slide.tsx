import { useEffect, useState } from "react";
import NavBarFichier from "../components/NavBarFichier";
import NavBarSlide from "../components/NavBarSlide";
import SlideView from "../components/SlideView";
import SlideService from "../services/SlideService";
import DiapoService from "../services/DiapoService";
import { useParams } from "react-router-dom";
import "../assets/css/ListSlide.css";

import LoadSVG from "../images/ball-triangle.svg";
import { DiaporamaData } from "../components/DiapoForm";
import { SlideData } from "../components/Slides";
import SlideItem from "../components/SlideItem";

const Slide = (): JSX.Element => {
  const params = useParams();
  const [menuParams, setMenuParams] = useState(false);
  const [visibleMenu, setVisibleMenu] = useState(false);
  const [visibleSlideMenu, setVisibleSlideMenu] = useState(false);
  const [idSlide, setIdSlide] = useState(null);
  const [slides, setSlide] = useState<SlideData[]>([]);
  const [ActiveSlide, setActiveSlide] = useState<SlideData>();
  const [loading, setLoading] = useState(false);
  const [diapo, setDiapo] = useState<DiaporamaData>();

  let contextMenu: any = HTMLElement;
  let scope: any = HTMLElement;
  let time:any;

  const menuParamsHandle = () => {
    setMenuParams(!menuParams);
  };

  const addSlide = () => {
    SlideService.store(params.id ? params.id : "").then((data: any) => {
      console.log(data)
      const slide: SlideData = {
        id: data.Slide.id,
        number: data.Slide.number,
        content: data.Slide.content,
        svg: data.Slide.svg,
        backgroundColor: data.Slide.backgroundColor || '#ffffff',
        idDiapo: data.Slide.idDiapo,
      };

      setSlide((prevSlide) => {
        return [...prevSlide, slide];
      });

      setActiveSlide(slide)
    });
  };

  const removeSlide = () => {
    if (idSlide) {
      SlideService.delete(slides[idSlide].id).then((data: any) => {
        console.log(data);
      });
      slides.splice(idSlide, 1);
    }
    setVisibleMenu(false);
    setVisibleSlideMenu(false);
    setActiveSlide(slides[0]);
  };

  const editSlideMenu = () => {
    setVisibleMenu(false);
    setVisibleSlideMenu(false);
    setMenuParams(true);
  };

  const activeSlide = (data: any) => {
    setActiveSlide(slides[data]);
  };

  const updateSlideList = (data: any) => {
    const newSlide = slides.map((slide) => {
      if (slide.id !== data.id) {
        return slide;
      }

      return {
        ...slide,
        svg: data.svg,
        content: data.json,
      };
    });
    setSlide(newSlide);
    inactivityTime(newSlide)
  };

  const inactivityTime = (data: SlideData[]) => {
    clearTimeout(time);
    time = setTimeout(() => {
      SlideService.saveAll(data).then((data) => {
        // console.log(data)
      })
    }, 3000)
  };

  const resetTimer = () => {
    clearTimeout(time);
  }

  const saveAllSlide = () => {
    return SlideService.saveAll(slides).then((data) => {
      return data
    })
  }

  const normalizePozition = (mouseX: number, mouseY: number) => {
    scope = document.getElementById("scope");
    let { left: scopeOffsetX, top: scopeOffsetY } =
      scope.getBoundingClientRect();
    scopeOffsetX = scopeOffsetX < 0 ? 0 : scopeOffsetX;
    scopeOffsetY = scopeOffsetY < 0 ? 0 : scopeOffsetY;
    const scopeX = mouseX - scopeOffsetX;
    const scopeY = mouseY - scopeOffsetY;
    return { scopeY, scopeX };
  };
  const MenuShow = (data: any) => {
    const { clientX: mouseX, clientY: mouseY } = data.event;
    const { scopeX, scopeY } = normalizePozition(mouseX, mouseY);
    contextMenu = document.getElementById("context-menu");
    setVisibleMenu(false);
    contextMenu.style.top = `${scopeY}px`;
    contextMenu.style.left = `${scopeX}px`;
    setTimeout(() => {
      setIdSlide(data.idSlide);
      setVisibleMenu(true);
    }, 50);
  };
  const addObject = (data: any) => {
    console.log(data);
  };
  useEffect(() => {
    setLoading(true);
    contextMenu = document.getElementById("context-menu");
    scope = document.getElementById("scope");
    scope.addEventListener("click", (e: { target: { offsetParent: any } }) => {
      let slideMenu = document.getElementById("slide-menu");
      if (e.target.offsetParent !== contextMenu) {
        setVisibleMenu(false);
      }
      if (e.target.offsetParent !== slideMenu) {
        setVisibleSlideMenu(false);
      }
    });

    DiapoService.getOneDiapo(params.id ? params.id : "")
      .then((diapo) => {
        const diaporama: DiaporamaData = {
          id: diapo.idDiapo,
          title: diapo.titre,
          modifiedDate: new Date(diapo.updated_at),
          pageCount: diapo.nbrDiapo,
          svg: diapo.svg,
        };

        setDiapo(diaporama);
        const slides = diapo.slides;
        slides.forEach((data: any) => {
          const slide: SlideData = {
            id: data.id,
            number: data.number,
            content: data.content,
            svg: data.svg,
            backgroundColor: data.backgroundColor,
            idDiapo: data.idDiapo,
          };
          setSlide((prevSlide) => {
            return [...prevSlide, slide];
          });
        });
        setActiveSlide(slides[0]);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <div className="w-full h-screen bg-gray-100/50 flex  justify-center items-center">
        <img src={LoadSVG} />
      </div>
    );
  }

  return (
    <div className="w-full h-screen bg-gray-100/50" id="scope">
      <NavBarFichier titre={diapo?.title} date={diapo?.modifiedDate.toLocaleString()} saveAllSlide={saveAllSlide} id={params.id}/>
      <NavBarSlide
        menuParamsHandle={menuParamsHandle}
        menuParams={menuParams}
        addSlide={addSlide}
        addObject={addObject}
      />
      <div className="relative h-[89%] flex flex-row">
        <div className="relative w-[15%] border-r h-full flex flex-col items-center py-4 overflow-y-auto">
          {slides.map((slide: any, index: number) => (
            <SlideItem
              backgroundColor={slide.backgroundColor}
              index={index}
              id={slide.id}
              number={slide.number}
              svg={slide.svg}
              MenuShowSlide={MenuShow}
              activeSlideItem={activeSlide}
              idSlideActive={ActiveSlide?.id}
              key={slide.id}
            />
          ))}
        </div>
        <div className="w-[85%] bg-gray-200 h-full flex flex-row">
          <div
            className={`${
              menuParams ? "w-[80%]" : "w-full"
            } h-full duration-300 ease-in`}
          >
            {!loading && ActiveSlide && (
              <SlideView
                slide={ActiveSlide}
                key={ActiveSlide.id}
                updateSlideList={updateSlideList}
                visibleSlideMenu={visibleSlideMenu}
                setVisibleSlideMenu={setVisibleSlideMenu}
                resetTimer={resetTimer}
              />
            )}
          </div>
          <div
            className={`${
              menuParams ? "w-[20%]" : "w-0"
            } h-full bg-white border-l flex justify-center items-center duration-300 ease-in`}
          ></div>
        </div>
      </div>
      <div
        id="context-menu"
        className={`absolute z-[10000] w-[150px] bg-white border shadow ${
          visibleMenu ? "scale-1" : "scale-0"
        } origin-top-left ease-in-out duration-200`}
      >
        <div
          className="py-2 px-3 cursor-pointer hover:bg-gray-300 text-sm"
          onClick={editSlideMenu}
        >
          Modifier
        </div>
        <div
          className="py-2 px-3 cursor-pointer hover:bg-gray-300 text-sm border-t"
          onClick={removeSlide}
        >
          Supprimer
        </div>
      </div>
    </div>
  );
};

export default Slide;

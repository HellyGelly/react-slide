import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { DiaporamaData } from '../components/DiapoForm';
import { SlideData } from '../components/Slides';
import DiapoService from "../services/DiapoService";
import LoadSVG from "../images/ball-triangle.svg";

const Slideshow = (): JSX.Element => {
  const params = useParams();
  const [loading, setLoading] = useState(false);
  const [diapo, setDiapo] = useState<DiaporamaData>();
  const [slides, setSlide] = useState<SlideData[]>([]);
  const [ActiveSlide, setActiveSlide] = useState<SlideData>();
  const [number, setNumber] = useState(0)

  const suivantSlide = () => {
    if (slides.length > number + 1) {
      setActiveSlide(slides[number + 1])
      setNumber(number + 1)
    }
  }

  const prevSlide = () => {
    if (0 <= number - 1) {
      setActiveSlide(slides[number - 1])
      setNumber(number - 1)
    }
  }

  const navigate = useNavigate();
  const closeView = () => {
    navigate(`/app/1/diapo/${params.id}/edit`)
  }


  useEffect(() => {
    setLoading(true);

    DiapoService.getOneDiapo(params.id ? params.id : "")
      .then((diapo) => {
        const diaporama: DiaporamaData = {
          id: diapo.idDiapo,
          title: diapo.titre,
          modifiedDate: new Date(diapo.updated_at),
          pageCount: diapo.nbrDiapo,
          svg: diapo.svg,
        };

        setDiapo(diaporama);
        const slides = diapo.slides;
        slides.forEach((data: any) => {
          const slide: SlideData = {
            id: data.id,
            number: data.number,
            content: data.content,
            svg: data.svg,
            backgroundColor: data.backgroundColor,
            idDiapo: data.idDiapo,
          };
          setSlide((prevSlide) => {
            return [...prevSlide, slide];
          });
        });
        setActiveSlide(slides[0])
      })
      .finally(() => {
        setLoading(false);
      });


    window.onkeydown = function (event) {
      switch (event.key) {
        case 'ArrowRight':
          suivantSlide()
          break;
        case 'ArrowLeft':
          prevSlide()
          break;
      }
    };
  }, []);

  if (loading) {
    return (
      <div className="w-full h-screen bg-gray-100/50 flex  justify-center items-center">
        <img src={LoadSVG} />
      </div>
    );
  }

  return (
    <div className='relative w-full h-screen bg-neutral-900' style={{ backgroundColor: ActiveSlide?.backgroundColor }} id={(ActiveSlide?.number)?.toString()} key={ActiveSlide?.id}>
      <img className='w-full h-full' src={ActiveSlide?.svg} />
      <div className='absolute top-4 left-4'>
        <button type='button' className='bg-neutral-800/50 border border-neutral-800 px-2 py-1 rounded hover:bg-neutral-800/75' onClick={closeView}>Close</button>
      </div>
      <div className='absolute left-4 bottom-4 flex flex-row gap-3'>
        <button type='button' className='bg-neutral-800/50 border border-neutral-800 px-2 py-1 rounded hover:bg-neutral-800/75' onClick={prevSlide}>Prev</button>
        <button type='button' className='bg-neutral-800/50 border border-neutral-800 px-2 py-1 rounded hover:bg-neutral-800/75' onClick={suivantSlide}>Suiv</button>
      </div>
      <div className='absolute bottom-4 right-4'>{number + 1}</div>
    </div>
  );
}

export default Slideshow;

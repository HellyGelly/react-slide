import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { DiaporamaData } from "../components/DiapoForm";
import "../App.css";
import DiapoService from "../services/DiapoService";
import LoadSVG from "../images/ball-triangle.svg";
import DiapoCard from "../components/DiapoCard";
import DiapoCardNew from "../components/DiapoCardNew";
import Search from "../images/search.svg";
import Table from "../images/apps.svg";
import List from "../images/bars-sort.svg";
import Sort from "../images/interlining.svg";

import Switcher from "../components/Switcher";
import DiapoCardNewList from "../components/DiapoCardNewList";

const Diapo = () => {
  const [diaporamas, setDiaporamas] = useState<DiaporamaData[]>([]);
  const [load, setLoad] = useState(true);
  const [addDiapo, setAddDiapo] = useState(false);
  const [inputAddDiapo, setInputAddDiapo] = useState("");
  const [menuNav, setMenuNav] = useState(false);
  const [newAffichage, setNewAffichage] = useState(false);
  const [listMode, setListMode] = useState(false);
  const [sortMode, setSortMode] = useState("DESC");
  const [inputSearchDiapo, setInputSearchDiapo] = useState("");

  const navigate = useNavigate();
  const ActionLink = (diapoId: number) => {
    navigate(`/app/1/diapo/${diapoId}/edit/`);
  };
  useEffect(() => {
    DiapoService.getDiapoByUser(1, sortMode, "")
      .then((data) => {
        data.forEach((element: any) => {
          const diaporama: DiaporamaData = {
            id: element.id,
            title: element.titre,
            modifiedDate: new Date(element.updated_at),
            pageCount: element.nbrDiapo,
            svg: element.svg,
          };
          setDiaporamas((oldDiapo) => [...oldDiapo, diaporama]);
        });
      })
      .finally(() => {
        setLoad(false);
      });
  }, []);

  const updateSearch = (data:string) => {
    setInputSearchDiapo(data)
    DiapoService.getDiapoByUser(
      1,
      sortMode === "ASC" ? "DESC" : "ASC",
      inputSearchDiapo ? inputSearchDiapo : ''
    ).then((data) => {
      setDiaporamas([]);
      data.forEach((element: any) => {
        const diaporama: DiaporamaData = {
          id: element.id,
          title: element.titre,
          modifiedDate: new Date(element.updated_at),
          pageCount: element.nbrDiapo,
          svg: element.svg,
        };
        setDiaporamas((oldDiapo) => [...oldDiapo, diaporama]);
      });
    });
  };

  const showAddDiapo = () => {
    setAddDiapo(!addDiapo);
  };
  const closeAddDiapo = () => {
    setAddDiapo(false);
  };

  const menuNavHandle = () => {
    setMenuNav(!menuNav);
  };

  const newAffichageHandle = () => {
    setNewAffichage(!newAffichage);
  };

  const listModeHandle = () => {
    setListMode(!listMode);
  };

  const sortDiapo = () => {
    DiapoService.getDiapoByUser(1, sortMode === "ASC" ? "DESC" : "ASC", "")
      .then((data) => {
        setDiaporamas([]);
        data.forEach((element: any) => {
          const diaporama: DiaporamaData = {
            id: element.id,
            title: element.titre,
            modifiedDate: new Date(element.updated_at),
            pageCount: element.nbrDiapo,
            svg: element.svg,
          };
          setDiaporamas((oldDiapo) => [...oldDiapo, diaporama]);
        });
      })
      .finally(() => {
        sortMode === "ASC" ? setSortMode("DESC") : setSortMode("ASC");
      });
  };

  const createDiapo = () => {
    if (!inputAddDiapo) {
      return;
    }

    DiapoService.store(1, inputAddDiapo).then((data) => {
      const diaporama: DiaporamaData = {
        id: data.idDiapo,
        title: inputAddDiapo,
        modifiedDate: new Date(),
        pageCount: 1,
        svg: "https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png",
      };
      setDiaporamas((prevDiapo) => {
        return [diaporama, ...prevDiapo];
      });
      setInputAddDiapo("");
      setAddDiapo(false);
      navigate(`/app/1/diapo/${diaporama.id}/edit/`);
    });
  };

  if (load) {
    return (
      <div className="w-full h-screen bg-gray-100 dark:bg-neutral-700 flex justify-center items-center">
        <img src={LoadSVG} />
        <div className="hidden">
          <Switcher />
        </div>
      </div>
    );
  }

  return (
    <div className="relative diapo min-h-screen dark:bg-neutral-700" id="scope">
      {addDiapo && (
        <div className="z-50 fixed w-full h-screen flex justify-center items-center">
          <div
            className="absolute left-0 top-0 w-full h-full bg-gray-300/75"
            onClick={showAddDiapo}
          ></div>
          <div className="shadow rounded-md flex flex-col w-1/3 overflow-hidden bg-teal-400 text-white z-[99]">
            <div className="text-center pt-6 pb-6 text-3xl text-teal-800 font-extrabold italic">
              Veuillez mettre un titre de diapo
            </div>
            <div className="pb-6 pt-3 flex justify-center items-center px-12">
              <input
                className="p-3 outline-none text-black bg-teal-100 rounded-sm w-full"
                type="text"
                placeholder="Nom de la diapo"
                value={inputAddDiapo}
                onChange={(e) => setInputAddDiapo(e.target.value)}
              />
            </div>
            <div className="pb-6 pt-3 flex justify-center items-center">
              <button
                className="mr-3 p-3 outline-none text-white bg-violet-600 hover:bg-violet-700 rounded-sm w-1/3"
                onClick={createDiapo}
              >
                Créer
              </button>
              <button
                className="mr-3 p-3 outline-none text-white bg-violet-600 hover:bg-violet-700 rounded-sm w-1/3"
                onClick={closeAddDiapo}
              >
                Annuler
              </button>
            </div>
          </div>
        </div>
      )}
      <div className="relative w-full h-[6%] flex flex-row justify-between items-center py-4 shadow">
        <div className="relative flex flex-col ml-4 py-1">
          <div className="flex flex-row items-center">
            <button
              type="button"
              className="p-2 bg-orange-400 hover:bg-orange-600 text-white rounded-xl text-sm"
              onClick={showAddDiapo}
            >
              Créer une diapo
            </button>
          </div>
        </div>
        <div className="relative mr-4 flex flex-row justify-center items-center">
          <div className="mr-4">
            <div className="relative w-8 h-8 rounded-full">
              <div
                className="w-8 h-8 hover:ring hover:ring-indigo-500/30 bg-indigo-500 text-white flex justify-center items-center cursor-pointer rounded-full"
                onClick={menuNavHandle}
              >
                A
              </div>
              {menuNav && (
                <div className="block absolute right-0 top-[115%] bg-white dark:bg-neutral-800 shadow-md z-[99] text-black w-fit whitespace-nowrap duration-300">
                  <div
                    className="py-2 px-4 text-sm text-neutral-500 dark:text-white dark:hover:bg-neutral-500 hover:bg-gray-300 hover:text-black cursor-pointer"
                    onClick={newAffichageHandle}
                  >
                    new Affichage
                  </div>
                  <div className="flex flex-row w-full cursor-default text-neutral-500 dark:text-white">
                    <div className="text-sm py-2 px-4">Mode :</div>
                    <Switcher />
                  </div>
                  <div className="py-2 px-4 text-sm bg-red-400 hover:bg-red-500 text-white">
                    Déconnexion
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="relative text-center h-32 bg-teal-200 dark:bg-indigo-700 text-4xl shadow overflow-hidden">
        <div className="absolute left-52 top-1/2 w-24 h-24 rounded-full bg-teal-500 dark:bg-indigo-500 -rotate-6"></div>
        <div className="absolute right-52 top-1/3 w-32 h-32 rounded-full bg-teal-500 dark:bg-indigo-500 rotate-12"></div>
        <div className="absolute left-1/2 bottom-2/4 w-32 h-32 rounded-full bg-teal-500 dark:bg-indigo-500 rotate-12 -translate-x-64 translate-y-3"></div>
        <div className="absolute inset-0 flex items-center justify-center">
          <div className="text-8xl text-center uppercase italic text-teal-700 dark:text-indigo-300 font-extrabold">
            Mes diapos !
          </div>
        </div>
      </div>

      {newAffichage && (
        <div className="relative w-full grid grid-cols-3 gap-6 px-32 py-24">
          {diaporamas.map((diaporama) => (
            <DiapoCard
              actionLink={ActionLink}
              key={diaporama.id}
              diaporama={diaporama}
            />
          ))}
        </div>
      )}
      {!newAffichage && (
        <div className="relative w-full">
          <div className="w-full flex justify-center items-center my-8">
            <div className="relative flex flex-row w-2/4">
              <input
                className="w-full py-3 px-4 outline-none bg-slate-300 dark:bg-neutral-800 text-neutral-500 dark:text-white rounded-sm"
                placeholder="Rechercher une présentation"
                value={inputSearchDiapo}
                onChange={(e) => updateSearch(e.target.value)}
              />
              <div className="absolute top-0 right-0 h-full flex justify-center items-center w-fit px-4 bg-slate-300 dark:bg-neutral-800">
                <img
                  className="w-6 h-6 dark:invert cursor-pointer"
                  src={Search}
                />
              </div>
            </div>
          </div>
          <div className="w-full flex justify-center items-center my-8">
            <div className="relative flex flex-row justify-center items-center gap-6">
              <div
                className="group relative w-10 h-10 flex justify-center items-center bg-slate-300 hover:bg-slate-400 dark:bg-neutral-800 rounded-full cursor-pointer dark:hover:bg-neutral-900"
                onClick={listModeHandle}
              >
                {listMode && (
                  <>
                    <img className="w-5 h-5 dark:invert" src={Table} />
                    <div className="group-hover:block hidden absolute w-2 h-2 rotate-45 bg-neutral-900 top-[110%] left-1/2 -translate-x-1 -translate-y-1"></div>
                    <div className="group-hover:block hidden absolute top-[110%] bg-neutral-900 w-fit text-white whitespace-nowrap px-2 py-1 text-sm">
                      Mode Grille
                    </div>
                  </>
                )}
                {!listMode && (
                  <>
                    <img className="w-5 h-5 dark:invert" src={List} />
                    <div className="group-hover:block hidden absolute w-2 h-2 rotate-45 bg-neutral-900 top-[110%] left-1/2 -translate-x-1 -translate-y-1"></div>
                    <div className="group-hover:block hidden absolute top-[110%] bg-neutral-900 w-fit text-white whitespace-nowrap px-2 py-1 text-sm">
                      Mode List
                    </div>
                  </>
                )}
              </div>
              <div
                className="group relative w-10 h-10 flex justify-center items-center bg-slate-300 hover:bg-slate-400 dark:bg-neutral-800 rounded-full cursor-pointer dark:hover:bg-neutral-900"
                onClick={sortDiapo}
              >
                <img className="w-5 h-5 dark:invert" src={Sort} />
                <div className="group-hover:block hidden absolute w-2 h-2 rotate-45 bg-neutral-900 top-[110%] left-1/2 -translate-x-1 -translate-y-1"></div>
                <div className="group-hover:block hidden absolute top-[110%] bg-neutral-900 w-fit text-white whitespace-nowrap px-2 py-1 text-sm">
                  Trier par Date
                </div>
              </div>
            </div>
          </div>
          {!listMode && (
            <div className="relative w-full grid grid-cols-5 gap-6 px-32 pt-12 pb-24">
              {diaporamas.map((diaporama) => (
                <DiapoCardNew
                  actionLink={ActionLink}
                  key={diaporama.id}
                  diaporama={diaporama}
                />
              ))}
            </div>
          )}
          {listMode && (
            <div className="relative w-full flex flex-col gap-2 px-32 pt-12 pb-24">
              {diaporamas.map((diaporama) => (
                <DiapoCardNewList
                  actionLink={ActionLink}
                  key={diaporama.id}
                  diaporama={diaporama}
                />
              ))}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Diapo;

import { Route, Routes } from 'react-router-dom'
import './App.css';
import Diapo from './pages/Diapo';
import Home from './pages/Home';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Register from './pages/Register';
import Slide from './pages/Slide';
import Slideshow from './pages/Slideshow';

const App = (): JSX.Element => {
  return (
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='/register' element={<Register />} />
      <Route path='/login' element={<Login />} />
      <Route path='/app' element={<Diapo />} />
      <Route path='/app/:username/diapo/:id/edit' element={<Slide />} />
      <Route path='/app/:username/diapo/:id' element={<Slideshow />} />
      <Route path='*' element={<NotFound />} />
    </Routes>
  );
}

export default App;

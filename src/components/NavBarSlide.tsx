import Text from "../images/text.png";
import Plus from "../images/plus.png";
import Parametre from "../images/parametre.png";
import Close from "../images/close.png";
import Shapes from '../images/shapes.png';


const NavBarSlide = (data: any): JSX.Element => {
  return (
    <div className="relative w-full border-b  h-[5%] flex flex-row justify-between items-center">
      <div className="flex flex-row">
        <div
          className="group relative mx-2 w-7 h-7 hover:bg-gray-300/50 border rounded-sm cursor-pointer flex justify-center items-center"
          onClick={data.addSlide}
        >
          <img
            src={Plus}
            alt="Créer une zone de texte"
            className="w-5 h-5 p-1"
          />
          <div className="absolute hidden group-hover:block bg-neutral-800 left-1/2 -translate-x-1 bottom-0 translate-y-1 w-2 h-2 rotate-45"></div>
          <div className="absolute -left-1 top-full bg-neutral-800 whitespace-nowrap hidden group-hover:flex justify-center items-center text-white text-xs py-1 px-2 z-50">
            Nouvelle slide
          </div>
        </div>
        <div className="group relative ml-4 mr-1 w-7 h-7 hover:bg-gray-300/50 rounded-sm cursor-pointer flex justify-center items-center"
        onClick={() => {data.addObject('text')}}>
          <img
            src={Text}
            alt="Créer une zone de texte"
            className="w-7 h-7 p-1"
          />
          <div className="absolute hidden group-hover:block bg-neutral-800 left-1/2 -translate-x-1 bottom-0 translate-y-1 w-2 h-2 rotate-45"></div>
          <div className="absolute -left-1 top-full bg-neutral-800 hidden group-hover:flex whitespace-nowrap justify-center items-center text-white text-xs py-1 px-2 z-50">
            Créer une zone de texte
          </div>
        </div>
        <div className="group relative mx-1 w-7 h-7 hover:bg-gray-300/50 rounded-sm cursor-pointer flex justify-center items-center"
        onClick={() => {data.addObject('form')}}>
          <img
            src={Shapes}
            alt="Créer une zone de texte"
            className="w-6 h-6 p-1"
          />
          <div className="group-hover:block hidden absolute top-full left-0 w-32 h-fit bg-white z-50 shadow text-sm border">
            <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('rectangle')}}>Rectangle</div>
            <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('triangle')}}>Triangle</div>
            <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('line')}}>Line</div>
            {/* <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('polygon')}}>Polygon</div> */}
            <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('circle')}}>Circle</div>
            {/* <div className="px-2 py-1 hover:bg-gray-200" onClick={() => {data.addObject('polyline')}}>Polyline</div> */}
          </div>
        </div>
      </div>
      <div className="flex flex-row">
        {data.menuParams && (
          <div
            className="group relative mx-2 w-7 h-7 hover:bg-gray-300/50 border rounded-sm cursor-pointer flex justify-center items-center"
            onClick={data.menuParamsHandle}
          >
            {!data.menuParams && (
              <img
                src={Parametre}
                alt="Créer une zone de texte"
                className="w-5 h-5 p-1"
              />
            )}
            {data.menuParams && (
              <img
                src={Close}
                alt="Créer une zone de texte"
                className="w-5 h-5 p-1"
              />
            )}
            <div className="absolute hidden group-hover:block bg-neutral-800 left-1/2 -translate-x-1 bottom-0 translate-y-1 w-2 h-2 rotate-45"></div>
            <div className="absolute -right-1 top-full bg-neutral-800 w-32 hidden group-hover:flex justify-center items-center text-white text-xs py-1 z-50">
              Fermer les paramètres
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default NavBarSlide;

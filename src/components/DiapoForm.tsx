import React from "react"; 

export interface DiaporamaData {
    id: number, 
    title: string;
    modifiedDate: Date;
    pageCount: number;
    svg : string
}
  
interface DiaporamaProps {
    data: DiaporamaData;
}
  
const DiapoForm: React.FC<DiaporamaProps> = ({ data }) => {
    return (
      <div>
        <h2>{data.title}</h2> 
        <p>Dernière modification : {data.modifiedDate.toLocaleString()}</p>  
        <p>Nombre de pages : {data.pageCount}</p>
      </div>
    );
};


export default DiapoForm;
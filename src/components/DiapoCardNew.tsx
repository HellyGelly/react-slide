import { DiaporamaData } from "./DiapoForm";
import menu from '../images/menu-dots-vertical.svg';


interface DiapoCardProps { diaporama: DiaporamaData, actionLink: (idDiapo: number) => void };
const DiapoCardNew = ({ diaporama, actionLink }: DiapoCardProps): JSX.Element => {
    return (
        <div onClick={() => { actionLink(diaporama.id) }} className='hover:ring hover:ring-indigo-600/50 overflow-hidden relative group p-0 cursor-pointer shadow-md'>
            <div className='w-full h-48 m-0' style={{ padding: '0px' }}>
                {diaporama.svg &&
                    <img src={diaporama.svg}
                        className='w-full h-full dark:bg-neutral-800' />
                }
                {!diaporama.svg &&
                    <div
                        className='w-full h-full dark:bg-neutral-800' />
                }
            </div>
            <div className='absolute left-0 bottom-0 h-fit py-2 border-t border-neutral-300 dark:border-neutral-900 bg-neutral-300 dark:bg-neutral-900 dark:text-white w-full duration-200 ease-in-out'>
                <div className='py-1 px-2' >{diaporama.title}</div>
                <div className='py-1 px-2 text-xs dark:text-white '>
                    {diaporama.modifiedDate.toLocaleDateString()}
                </div>
            </div>
            {/* <div className="absolute right-2 bottom-2 flex flex-col justify-center items-center w-8 h-8 rounded-full hover:bg-neutral-400 cursor-pointer dark:hover:bg-neutral-800">
                <img className="w-4 h-4 dark:invert" src={menu} />
            </div> */}
        </div>
    );
};

export default DiapoCardNew
import { useEffect } from "react";

const SlideItem = (data: any): JSX.Element => {
  useEffect(() => {
    const scopeSlide: any = document.getElementById(data.index);

    scopeSlide.addEventListener(
      "contextmenu",
      (event: any) => {
        event.preventDefault();
        data.MenuShowSlide({ event, idSlide: data.index });
      }
    );
  }, data.slide);


  const activeSlide = () => {
    data.activeSlideItem(data.index)
  }

  return (
    <div
      className={`relative border w-8/12 min-h-[7rem] ${data.idSlideActive === data.id ? 'ring ring-violet-500' : 'hover:ring'} hover:ring-indigo-400 cursor-pointer mb-4`}
      style={{ backgroundColor: data.backgroundColor }}
      id={data.index}
      onClick={activeSlide}
    >
      {data.svg && (
        <img src={data.svg} className="w-full h-full" />
      )}
      <div className="absolute right-1 bottom-0">{data.number}</div>
    </div>
  );
};

export default SlideItem;

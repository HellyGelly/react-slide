import { useEffect, useState, useRef, createRef } from "react";
import { fabric } from "fabric";
import { Canvas, Object } from "fabric/fabric-impl";
import SlideMenu from "./SlideMenu";
import { ColorResult, PhotoshopPicker } from "react-color";

interface props {
  slide: any,
  updateSlideList: (data: any) => void,
  visibleSlideMenu: boolean,
  resetTimer: () => void,
  setVisibleSlideMenu: React.Dispatch<React.SetStateAction<boolean>>
}

const SlideView = (data: props): JSX.Element => {

  const [menuPos, setMenuPos] = useState({ x: 0, y: 0 });
  const [menuTarget, setMenuTarget] = useState<fabric.Object | undefined>();
  const slide = createRef<HTMLDivElement>();
  const [visibleColorPicker, setVisibleColorPicker] = useState(false);
  const [nameColor, setNameColor] = useState<string>('');
  const [color, setColor] = useState<string | undefined>('');
  const [objectModif, setObjectModif] = useState<Object>();

  var canvas = useRef<Canvas>();

  const objDetails: any = {
    borderColor: '#3b73e4',
    cornerColor: 'red',
    cornerStrokeColor: '#3b73e4',
    cornerStyle: 'circle',
    cornerSize: 10,
  }

  useEffect(() => {
    canvas.current = new fabric.Canvas('canvas', {
      fireRightClick: true,
      fireMiddleClick: true,
      stopContextMenu: true,
    });

    if (slide)
      canvas.current.setDimensions({
        width: slide.current?.clientWidth || 0,
        height: slide.current?.clientHeight || 0,
      });

    if (data.slide.content) {
      canvas.current.loadFromJSON(data.slide.content, function () {
        if (canvas.current) canvas.current.renderAll();
      }, function (o: any, object: any) {
        // console.log(o, object)
      })
    }

    let parent = data;
    
    canvas.current.on('object:modified', () => {
      parent.updateSlideList({ svg: rasterizeSVG(), id: parent.slide.id, json: canvas.current ? canvas.current.toJSON() : "" })
    })

    const rasterizeSVG = () => {
      return 'data:image/svg+xml;utf8,' + encodeURIComponent(canvas.current ? canvas.current.toSVG() : "")
    }

    function handleResize() {
      if (canvas.current)
        canvas.current.setDimensions({
          width: slide.current?.clientWidth || 0,
          height: slide.current?.clientHeight || 0,
        });
    }

    canvas.current.on('mouse:down', (event) => {
      if(event.button === 1) {
        console.log("left click");
      }
      if(event.button === 2) {
        console.log("middle click");
      }
      if(event.button === 3) {
        console.log("right click");
        if (event.pointer) {
          setMenuTarget(event.target);
          setMenuPos(event.pointer);
          data.setVisibleSlideMenu(true);
        }
      }
      resetTime()
    })

    function resetTime() {
      data.resetTimer();
    }

    window.addEventListener("resize", handleResize);
  }, [data.slide]);

  const capitalizeFirstLetter = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  const createObject = (obj: string, position: any) => {
    if (canvas.current)
      switch (obj) {
        case "text":
          const textbox = new fabric.Textbox('Your text here', {
            left: position.x,
            top: position.y,
            ...objDetails,
          });
          canvas.current.add(textbox);
          break;
        case "rectangle":
          const rect = new fabric.Rect({
            left: position.x,
            top: position.y,
            width: 40,
            height: 40,
            fill: 'green',
            ...objDetails,
          });
          canvas.current.add(rect);
          break;
        case "triangle":
          const triangle = new fabric.Triangle({
            left: position.x,
            top: position.y,
            width: 40,
            height: 40,
            angle: 0,
            fill: 'green',
            ...objDetails,
          });
          canvas.current.add(triangle);
          break;
        case "circle":
          const circle = new fabric.Circle({
            left: position.x,
            top: position.y,
            radius: 20,
            fill: 'green',
            ...objDetails,
          });
          canvas.current.add(circle);
          break;
        case "line":
          const line = new fabric.Line([position.x, position.y, position.x + 100, position.y], {
            stroke: 'green'
          });
          canvas.current.add(line);
          break;
      }
      data.setVisibleSlideMenu(false);
  }

  const modifyObject = (name: string, object: Object) => {
    if (canvas.current)
      switch (name) {
        case "fill":
          setColor(object.fill?.toString());
          setObjectModif(object);
          setVisibleColorPicker(true);
          break;
      }
      data.setVisibleSlideMenu(false);
  }

  const applyColor = () => {
    console.log(color)
    if (objectModif)
      objectModif.set('fill', color);
    stopObjectModify();
  }

  const stopObjectModify = () => {
    canvas.current?.renderAll();
    setObjectModif(undefined);
    setNameColor('');
    setColor('');
    setVisibleColorPicker(false);
  }

  return (
    <div className="relative w-full h-full flex flex-col">
      <div className="w-full h-[97%] flex justify-center items-center p-10">
        <div
          className="relative w-full h-full shadow"
          style={{ backgroundColor: data.slide.backgroundColor }}
          ref={slide}
        >
          <canvas id='canvas' />
          {data.visibleSlideMenu &&
            <SlideMenu position={menuPos}
              createObject={createObject}
              modifyObject={modifyObject}
              target={menuTarget}
              canvas={canvas.current}
            />
          }
          {visibleColorPicker &&
            <div className="w-full h-full absolute top-0 left-0 flex justify-center items-center">
              <PhotoshopPicker
                header={capitalizeFirstLetter(nameColor) + " Color"}
                color={color}
                onChangeComplete={(color) => setColor(color.hex)}
                onAccept={() => applyColor()}
                onCancel={() => stopObjectModify()}
              />
            </div>
          }
        </div>
      </div>
      <div className="w-full h-[3%] flex justify-end text-xs items-center bg-white border-t">
        <div className="px-1">Slide {data.slide.id}</div>
      </div>
    </div>
  );
};

export default SlideView;

import React from "react"; 

export interface SlideData {
    id: number;
    backgroundColor: string;
    content: string;
    number: number;
    idDiapo: number;
    svg: string;
}
  
interface SlideProps {
    data: SlideData;
}
  
const SlideForm: React.FC<SlideProps> = ({ data }) => {
    return (
      <div>
        <h2>{data.id}</h2> 
      </div>
    );
};


export default SlideForm;
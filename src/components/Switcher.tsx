
import { useState } from "react";
import useDarkSide from "../hooks/useDarkSide";
import moon from '../images/moon-stars.svg';
import sun from '../images/moon-stars-full.svg';

export default function Switcher() {
    const dark = useDarkSide();
    const [darkSide, setDarkSide] = useState(
        dark.colorTheme === "light" ? true : false
    );

    const toggleDarkMode = (checked: boolean | ((prevState: boolean) => boolean)) => {
        dark.setTheme(dark.colorTheme);
        setDarkSide(checked);
    };

    return (
        <button type="button" onClick={() => { toggleDarkMode(darkSide) }}>
            {dark.colorTheme === 'dark' && (
                <img className="w-5 h-5" src={sun} />
            )}
            {dark.colorTheme === 'light' && (
                <img className="w-5 h-5 filter invert" src={moon} />
            )}
        </button>
    );
}
import { Card, CardContent, Typography } from "@material-ui/core";
import { DiaporamaData } from "./DiapoForm";


interface DiapoCardProps { diaporama: DiaporamaData, actionLink: (idDiapo: number) => void };
const DiapoCard = ({ diaporama, actionLink}: DiapoCardProps): JSX.Element => { 
    return (
        <Card onClick={() => {actionLink(diaporama.id)}} className='hover:ring hover:ring-indigo-600/50 overflow-hidden relative group p-0 cursor-pointer'>
            <CardContent className='w-full h-64 border-b m-0' style={{ padding: '0px' }}>
                {diaporama.svg &&
                    <img src={diaporama.svg}
                        className='w-full h-full' />
                }
                {!diaporama.svg &&
                    <img src='https://cdn.pixabay.com/photo/2012/04/13/12/30/new-32199_1280.png'
                        className='w-full h-full' />
                }
            </CardContent>
            <CardContent className='py-8 absolute left-0 top-full h-32 translate-y-0 group-hover:-translate-y-32 bg-white/75 dark:bg-neutral-700/75 dark:text-white w-full duration-200 ease-in-out'>
                <Typography variant="h5" className='text-center py-1' >{diaporama.title}</Typography>
                <Typography variant="body2" color="textSecondary" className='text-center py-1 dark:text-white '>
                    Dernière modification : {diaporama.modifiedDate.toLocaleString()}
                </Typography>
                <Typography variant="body2" color="textSecondary" className='text-center py-1 dark:text-white'>
                    Nombre de pages : {diaporama.pageCount}
                </Typography>
            </CardContent>
        </Card>
    );
};

export default DiapoCard
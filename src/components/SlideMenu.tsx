import { Object } from "fabric/fabric-impl";
import { createRef, useEffect, useState } from "react";
import { SketchPicker } from 'react-color';
import "../styles/SlideMenu.css";

interface option {
  name: string;
  content: string;
  fct: Function;
}

interface props {
  position: {
    x: number,
    y: number,
  },
  createObject: (obj: string, position: any) => void,
  modifyObject: (name: string, object: Object, param: any) => void,
  target: fabric.Object | undefined,
  canvas: fabric.Canvas | undefined,
}

const SlideMenu = ({position, createObject, modifyObject, target, canvas}: props): JSX.Element => {

  const defaultOptions: Array<option> = [
    { name: "text", content: "New Text", fct: createObject },
    { name: "rectangle", content: "New Rectangle", fct: createObject },
    { name: "triangle", content: "New Triangle", fct: createObject },
    { name: "circle", content: "New Circle", fct: createObject },
    { name: "line", content: "New Line", fct: createObject },
  ]
  const tolerence = 15;

  const menu1 = createRef<HTMLDivElement>();
  const [pos, setPos] = useState(position);
  const [optionsCreate, setOptionsCreate] = useState<Array<option>>(defaultOptions);
  const [optionsModify, setOptionsModify] = useState<Array<option>>([]);
  const [toLeft, setToLeft] = useState(false);
  const [toTop, setToTop] = useState(false);
  const [menuSize, setMenuSize] = useState({width: 0, height: 0});

  useEffect(() => {
    if (menu1.current) {
      setMenuSize({
        width: menu1.current.offsetWidth,
        height: menu1.current.offsetHeight,
      })
    }
  }, [])

  useEffect(() => {
    setPos(position);
    if (canvas) {
      setToLeft(canvas.getWidth() - tolerence < position.x + menuSize.width);
      setToTop(canvas.getHeight() - tolerence < position.y + menuSize.height);
    }
    if (target) {
      let type = target.get('type');
      let newOptions: Array<option> = new Array(0);
      newOptions.push({ name: "fill", content: "Fill color", fct: modifyObject })
      console.log(type)
      setOptionsModify(newOptions);
      setOptionsCreate([]);
      const colorPicker = document.createElement('input');
      colorPicker.type = 'color';
    } else {
      setOptionsCreate(defaultOptions);
      setOptionsModify([]);
    }
  }, [position]);

  useEffect(() => {
    if (canvas) {
      setToLeft(canvas.getWidth() - tolerence < position.x + menuSize.width);
      setToTop(canvas.getHeight() - tolerence < position.y + menuSize.height);
    }
  }, [menuSize]);

  const direction = () => {
    return toLeft ? (toTop ? "bottom-right" : "top-right") : (toTop ? "bottom-left" : "top-left")
  }

  return (
    <div id="slide-menu" onContextMenu={(e) => e.preventDefault()}
    style={{top: toTop ? pos.y - menuSize.height : pos.y, left: toLeft ? pos.x - menuSize.width : pos.x}}
    className={`${direction()}`}>
      <div ref={menu1} className={`options ${direction()}`}>
        {optionsCreate.map((option, index) => (
          <div key={index + option.name} className={`option ${index !== 0 ? "option-border" : ""}`}
          onClick={() => {
            if (target) option.fct(option.name, target);
            else option.fct(option.name, pos);
          }}>
            <p>{option.content}</p>
          </div>
        ))}
        {optionsModify.map((option, index) => (
          <div key={index + option.name} className={`option ${index !== 0 ? "option-border" : ""}`}
          onClick={() => {
            if (target) option.fct(option.name, target);
            else option.fct(option.name, pos);
          }}>
            <p>{option.content}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default SlideMenu;

import { DiaporamaData } from "./DiapoForm";


interface DiapoCardProps { diaporama: DiaporamaData, actionLink: (idDiapo: number) => void };
const DiapoCardNewList = ({ diaporama, actionLink }: DiapoCardProps): JSX.Element => {
    return (
        <div onClick={() => { actionLink(diaporama.id) }} className='hover:ring hover:ring-indigo-600/50 overflow-hidden relative group p-0 cursor-pointer shadow-md'>
            <div className='flex flex-row items-center justify-between py-2 border-t border-neutral-300 dark:border-neutral-900 bg-neutral-300 dark:bg-neutral-900 dark:text-white w-full duration-200 ease-in-out'>
                <div className='py-1 px-2' >{diaporama.title}</div>
                <div className='py-1 px-2 text-xs dark:text-white '>
                    {diaporama.modifiedDate.toLocaleDateString()}
                </div>
            </div>
        </div>
    );
};

export default DiapoCardNewList
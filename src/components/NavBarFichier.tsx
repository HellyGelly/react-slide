import Start from "../images/bouton-de-lecture-video.png";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Switcher from "./Switcher";

const NavBarFichier = (diapo: any): JSX.Element => {
  const [save, setSave] = useState(0);
  const [menuNav, setMenuNav] = useState(false);

  const menuNavHandle = () => {
    setMenuNav(!menuNav);
  };

  const saveClick = () => {
    setSave(1);
    diapo.saveAllSlide().then((data: any) => {
      if (data === "good") {
        setSave(2);
        setTimeout(() => {
          setSave(0);
        }, 2000);
      }
    });
  };

  const navigate = useNavigate();
  const closeDiapo = () => {
    navigate(`/app`);
  };

  const pushView = () => {
    navigate(`/app/1/diapo/${diapo.id}`)
  }

  return (
    <div className="relative w-full border-b h-[6%] dark:bg-neutral-700 flex flex-row justify-between items-center">
      <div className="relative flex flex-col ml-4 py-1">
        <div className="flex flex-row items-center">
          <div className="text-neutral-600 text-sm dark:text-white">
            {diapo.titre}
            <span className="text-xs text-neutral-400 ml-4">
              Derniere Modification : {diapo.date}
            </span>
          </div>
          {save === 2 && (
            <div className="ml-2 flex justify-center items-center text-xs w-5 h-5 rounded-full text-white bg-green-500">
              ✓
            </div>
          )}
          {save === 1 && (
            <div className="ml-2 text-xs dark:text-white">
              Enregistrement...
            </div>
          )}
        </div>
        <div className="flex flex-row text-xs dark:text-white">
          <div className="relative group hover:bg-gray-300/50 dark:hover:bg-neutral-500 cursor-pointer p-1 mr-2">
            Fichier <span className="ml-1 text-[0.6rem]">▼</span>
            <div className="absolute hidden left-0 top-full w-32 h-fit bg-white dark:bg-neutral-800 z-50 shadow-md group-hover:flex flex-col text-sm">
              <div
                className="p-2 cursor-pointer hover:bg-gray-200 dark:hover:bg-neutral-500"
                onClick={saveClick}
              >
                Enregistrer
              </div>
              <div className="p-2 cursor-pointer hover:bg-gray-200 dark:hover:bg-neutral-500">
                Paramètres
              </div>
              <div className="p-2 cursor-pointer hover:bg-gray-200 dark:hover:bg-neutral-500">
                Export en PDF
              </div>
              <div
                className="p-2 cursor-pointer hover:bg-red-400"
                onClick={closeDiapo}
              >
                Fermer
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="relative mr-4 flex flex-row justify-center items-center">
        <div className="mr-4">
          <button
            type="button"
            className="py-1 px-2 text-sm bg-violet-500/75 text-white hover:bg-violet-500 rounded-md flex justify-center items-center"
            onClick={pushView}
          >
            <img
              src={Start}
              alt="Créer une zone de texte"
              className="w-4 h-4 mr-2 filter invert"
            />
            Diaporama
          </button>
        </div>
        <div className="mr-4">
          <div className="relative w-8 h-8 rounded-full">
            <div
              className="w-8 h-8 hover:ring hover:ring-indigo-500/30 bg-indigo-500 text-white flex justify-center items-center cursor-pointer rounded-full"
              onClick={menuNavHandle}
            >
              A
            </div>
            {menuNav && (
              <div className="block absolute right-0 top-[115%] bg-white dark:bg-neutral-800 shadow-md z-[99] text-black w-fit whitespace-nowrap duration-300">
                <div className="flex flex-row w-full cursor-default text-neutral-500 dark:text-white">
                  <div className="text-sm py-2 px-4">Mode :</div>
                  <Switcher />
                </div>
                <div className="py-2 px-4 text-sm bg-red-400 hover:bg-red-500 text-white">
                  Déconnexion
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBarFichier;

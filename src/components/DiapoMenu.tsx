import { createRef, useEffect, useState } from "react";
import "../styles/DiapoMenu.css";

interface option {
  content: string;
}

interface props {
  position: {
    x: number,
    y: number,
  },
  deleteDiapo: (idDiapo : number) => void,
  idDiapo: number;
}

const DiapoMenu = ({position, deleteDiapo, idDiapo}: props): JSX.Element => {

  const options: Array<option> = [
    { content: "Delete" },
  ]

  const menu1 = createRef<HTMLDivElement>();
  const [pos, setPos] = useState(position);

  useEffect(() => {
    setPos(position);
  }, [position]);

  return (
    <div id="diapo-menu" onContextMenu={(e) => e.preventDefault()} onClick={(e) => e.preventDefault()}
    style={{top: pos.y, left: pos.x}}>
      <div ref={menu1} className={`options`}>
        {options.map((option, index) => (
          <div key={index + option.content} className={`option ${index !== 0 ? "option-border" : ""}`}  onClick={() => deleteDiapo(idDiapo)}>
            <p>{option.content}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default DiapoMenu;
